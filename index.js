require('dotenv').config({ path: './config/config.env' });
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const morgan = require('morgan');
const { createServer } = require('http');

const jwt = require('jsonwebtoken');
const fsPromises = require('fs').promises;
const path = require('path');
const { hash, genSalt, compare } = require('bcryptjs');
const usersDB = {
  users: require('./data/users.json'),
  setUsers: function (data) {
    // console.log()
    this.users = data;
  },
};
// require('./utils/firebaseConfig');
const allowedOrigins = ['http://127.0.0.1:3000', 'http://localhost:3000'];
const corsOptions = {
  origin: (origin, callback) => {
    if (allowedOrigins.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  optionsSuccessStatus: 200,
};
const app = express();
app.use(cookieParser());

const ACCESS_TOKEN_SECRET = `5b04578ee8d2567785b2acd74b02aaee73b0ebe9c4ed46657f469a739c8ff82b17bb0e1a300bf9d215024d4c3c36174b24283440b80dd9e53cb48d1adecac80f`;
const REFRESH_TOKEN_SECRET = `7921e711545f9fe41dd1a6e3f3b10a9f25633b87fc8dc90987081733b4a807055670634461c8be757f789105fb76459a473033ed24fd7f7efbbd28863e8dbdc3`;
const middlewires = [
  express.json({ limit: process.env.JSON_FILE_LIMIT }),
  cors(corsOptions),
  express.urlencoded({ extended: false, limit: process.env.URLENCODED_LIMIT }),
  cookieParser(),
  morgan('dev'),
];

app.use(middlewires);
// app.options('*', cors(corsOptions));
const httpServer = createServer(app);

//Roters
app.get('/users', verifyJWT, (req, res) => {
  const users = usersDB.users.map(({ username }) => ({ username }));
  res.json(users);
});
app.get('/refresh', handleRefreshToken);
app.get('/logout', handleLogout);
app.post('/register', async (req, res) => {
  try {
    const user = req.body;
    if (!user?.username || !user?.password)
      return res
        .status(400)
        .json({ message: 'Username and password are required.' });

    const duplicate = usersDB.users.find(
      (person) => person.username === user.username
    );
    if (duplicate)
      return res.status(409).json({ message: `User already exists` });
    const password = await hash(user?.password, await genSalt(10));

    const newUser = { username: user.username, password };
    usersDB.setUsers([...usersDB.users, newUser]);

    await fsPromises.writeFile(
      path.join(__dirname, './', 'data', 'users.json'),
      JSON.stringify(usersDB.users)
    );
    // console.log(usersDB.users);
    res.status(201).json({ success: `New user ${user.username} created!` });
  } catch (e) {
    res.status(500).json({ message: 'Something went wrong' });
  }
});
app.post('/login', async (req, res) => {
  const { username, password } = req.body;
  // console.log(req.body, '====================');
  if (!username || !password)
    return res
      .status(400)
      .json({ message: 'Username and password are required.' });

  const foundUser = usersDB.users.find(
    (person) => person.username === username
  );
  if (!foundUser) return res.status(400).json({ message: 'User not exist.' });
  const checkPass = await compare(password, foundUser.password);
  if (!checkPass)
    return res.status(400).json({ message: 'Invalid credentials' });
  const othersUser = usersDB.users.filter(
    (person) => person.username !== username
  );
  const accessToken = jwt.sign({ username }, ACCESS_TOKEN_SECRET, {
    expiresIn: '30s',
  });
  const refreshToken = jwt.sign({ username }, REFRESH_TOKEN_SECRET, {
    expiresIn: '1d',
  });
  const currentUser = { ...foundUser, refreshToken };
  usersDB.setUsers([...othersUser, currentUser]);
  // console.log(usersDB.users);
  await fsPromises.writeFile(
    path.join(__dirname, './data/users.json'),
    JSON.stringify(usersDB.users)
  );

  res.cookie('auth_cook', refreshToken, {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000,
    sameSite: 'none',
    // sameSite:'None',
    secure: true,
  });
  res.status(200).json({ username, accessToken });
});
app.use('*', (req, res, _n) => {
  res.status(404).json(`${req.path} not found`);
});
app.use((err, _rq, res, _n) => {
  console.log('err Name', err.message);
  const message = err.message || `ServeR ErroR OccereD`;
  let status = err.status || 500;
  if (err.name === 'MongoServerError') status = 500;

  return res.status(status).json({
    message,
  });
});

//Database Connection
// if (process.env.MONGO_URI) DBConnect();
const PORT = process.env.PORT || 5051;
const server = httpServer.listen(PORT, () => {
  console.log(`Alhamdu lillah server is listening on port ${PORT}`);
});

// const foundUser = usersDB.users.find(
//   (person) => person.username === 'taher267'
// );
// console.log(foundUser);

function verifyJWT(req, res, next) {
  const authHeader = req.headers.authorization;
  if (!authHeader) return res.sendStatus(401);
  const token = authHeader.split('Bearer ')[1];
  jwt.verify(token, ACCESS_TOKEN_SECRET, (e, decode) => {
    if (e) return res.sendStatus(403);
    return next();
  });
}

function handleRefreshToken(req, res) {
  const cookies = req.cookies;
  if (!cookies?.auth_cook) return res.sendStatus(401);
  const refreshToken = cookies.auth_cook;
  console.log(cookies);
  const foundUser = usersDB.users.find(
    (person) => person.refreshToken === refreshToken
  );
  if (!foundUser) return res.sendStatus(403);

  jwt.verify(refreshToken, REFRESH_TOKEN_SECRET, (e, decode) => {
    if (e || foundUser.username !== decode.username) return res.sendStatus(403);
    const accessToken = jwt.sign(
      { username: decode.username },
      ACCESS_TOKEN_SECRET,
      {
        expiresIn: '30s',
      }
    );
    return res.json({ accessToken });
  });
}

function handleLogout(req, res) {
  const cookies = req.cookies;
  if (!cookies?.auth_cook) return res.sendStatus(202);
  const refreshToken = cookies.auth_cook;
  console.log(cookies);
  const foundUser = usersDB.users.find(
    (person) => person.refreshToken === refreshToken
  );
  if (!foundUser) return res.sendStatus(403);
  res.clearCookie('auth_cook', {
    httpOnly: true,
    sameSite: 'none',
    secure: true,
  });
  return res.sendStatus(204);
}

function credentials(req, res, next) {
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin))
    res.header('Access-Control-Allow-Credentials', true);
  next();
}
